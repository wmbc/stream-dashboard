**WMBC Stream Panel**

This is the dashboard used for broadcasters at WMBC to monitor the stream status and set the song titles.
It supports pulling of media information from various players on the target system as well as manual selection.

**System Requirements:**

- Linux (tested on Ubuntu 20.04 LTS)
- [butt](https://danielnoethen.de/butt/)
- [playerctl](https://manpages.debian.org/bullseye/playerctl/playerctl.1.en.html)

Installation Instructions:
1. Clone the Git repository into a directory of your choice.
2. Ensure the `gradlew` executable has execution permissions.
3. Execute the `./gradlew jlink` command inside the Git directory.
4. Copy the new files and folders located inside `build/image` to another directory for convenience. (This is the app.)

Launch Instructions:
1. Navigate to the second folder containing the app.
2. Navigate to the `bin` folder.
3. Execute the `StreamPanel` executable to launch. (Make sure it has execution permissions first)
4. It is suggested to create a convenience script to run this command (shell or batch, depending on your platform.)