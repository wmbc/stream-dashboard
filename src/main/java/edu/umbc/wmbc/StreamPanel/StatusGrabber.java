//StatusGrabber.java:
//Provide functions to ascertain if a stream or recording is "up" or not.
//This class is mainly called by their respective Task Helpers, which run this code on a schedule.

package edu.umbc.wmbc.StreamPanel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.http.*;
import java.net.URI;
import java.nio.Buffer;
import java.util.TimerTask;


public class StatusGrabber {
    private final String streamIP;
    private final int streamPort;
//Default constructor reverts to a localhost address with a port of 8000, Icecast and Shoutcast's default.
    public StatusGrabber() {
        super();
        this.streamIP = "127.0.0.1";
        this.streamPort = 8000;
    }



    //Overloaded constructor allows specifying custom IP and port numbers.
    public StatusGrabber(String ip, int port) {
        this.streamIP = ip;
        this.streamPort = port;
    }

    //streamUp: Creates a simple HTTP request to the configured IP and port number. If the HTTP request is successful
    //(ie. the HTTP status code returns 200), then "true" is returned. In any other case "false" is returned.
    public boolean streamServerUp() throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create("http://"+streamIP+":"+streamPort+"/index.html")).version(HttpClient.Version.HTTP_1_1).build();
        int resp = client.send(request, HttpResponse.BodyHandlers.ofString()).statusCode();
        return (resp == 200 || resp == 302);
    }


    //Found a sample code at https://stackabuse.com/executing-shell-commands-with-java/

    public boolean streamRunning() throws IOException {
        Process streamCheckProcess = Runtime.getRuntime().exec("butt -S");
        BufferedReader resultsReader = new BufferedReader(new InputStreamReader(streamCheckProcess.getInputStream()));
        String line = "";
        while ((line = resultsReader.readLine()) != null) {
            if(line.matches("connected: 1")){
                return true;
            }
        }
        resultsReader.close();
        return false;

    }
    public boolean recordingRunning() throws IOException{
        Process recordingCheckProcess = Runtime.getRuntime().exec("butt -S");
        BufferedReader resultsReader = new BufferedReader(new InputStreamReader(recordingCheckProcess.getInputStream()));
        String line = "";
        while ((line = resultsReader.readLine()) != null) {
            if(line.matches("recording: 1")){
                return true;
            }
        }
        resultsReader.close();
        return false;
    }
    public String currentTitle() throws IOException, NullPointerException {
        String title;
        String artist;
        Process retrieveTitleProcess = Runtime.getRuntime().exec("playerctl metadata title");
        BufferedReader resultsReader = new BufferedReader(new InputStreamReader(retrieveTitleProcess.getInputStream()));
        title = resultsReader.readLine();
        try{
        Process retrieveArtistProcess = Runtime.getRuntime().exec("playerctl metadata artist");
        BufferedReader artistResultsReader = new BufferedReader(new InputStreamReader(retrieveArtistProcess.getInputStream()));
        artist = artistResultsReader.readLine();

        if(title.isEmpty() || artist.isEmpty()){
            return "WMBC";
        }
        return title.concat(" - ").concat(artist);
        }catch(IOException e){
            return title;
        }
        
    }
}
