package edu.umbc.wmbc.StreamPanel;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.util.TimerTask;

public class StreamPingTaskHelper extends TimerTask {
    private String ip;
    private int port;
    private Circle circle;
    public StreamPingTaskHelper(String ip, int port, Circle streamStatus){
        this.ip = ip;
        this.port = port;
        this.circle = streamStatus;
    }
    @Override
    public void run() {
        circle.setFill(Color.YELLOW);
        StatusGrabber sg = new StatusGrabber(ip,port);
        try {
            if(sg.streamServerUp()){
                circle.setFill(Color.GREEN);
            }else{
                circle.setFill(Color.RED);
            }
        } catch (Exception e) {
            circle.setFill(Color.RED);
        }
    }
}
