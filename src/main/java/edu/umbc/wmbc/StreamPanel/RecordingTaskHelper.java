package edu.umbc.wmbc.StreamPanel;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.io.IOException;
import java.util.TimerTask;

public class RecordingTaskHelper extends TimerTask {
    private Circle circle;
    public RecordingTaskHelper(Circle circle){
        this.circle = circle;
    }
    @Override
    public void run() {
        circle.setFill(Color.YELLOW);
        StatusGrabber sg = new StatusGrabber();
        try {
            if (sg.recordingRunning()) {
                circle.setFill(Color.GREEN);
            } else {
                circle.setFill(Color.RED);
            }
        }catch(IOException e){
            circle.setFill(Color.ORANGE);
        }

    }
}
