//AutoSubmitHelper.java
//TimerTask thread which assists the program in

package edu.umbc.wmbc.StreamPanel;

import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;

import java.util.TimerTask;

public class AutoSubmitHelper extends TimerTask {
    private String currentTitle;
    private String detectedTitle;
    private Button submitBtn;
    private RadioButton autoDJ;
    public AutoSubmitHelper(Label currentTitle, Label detectedTitle, Button submit, RadioButton autoDJ){
        this.currentTitle = currentTitle.getText();
        this.detectedTitle = detectedTitle.getText();
        this.submitBtn = submit;
        this.autoDJ = autoDJ;
    }
    @Override
    public void run() {
        System.out.println(autoDJ.isSelected() + currentTitle + detectedTitle);
        if(autoDJ.isSelected() && !currentTitle.equals(detectedTitle)){
            currentTitle = detectedTitle;
            //We want to have these changes be executed by the Submit Button's logic (which executes the
            //command that actually updates the title on the server). JavaFX prevents updating or firing UI elements
            //outside the JavaFX application thread, so we must use the runLater() function to queue this request
            //for the platform to execute at its leisure sometime later. This block of code tells JavaFX to act like
            //the submit button has been pressed.
            Platform.runLater(
                    () -> {
                        submitBtn.fire();
                    }
            );

        }

    }
}
