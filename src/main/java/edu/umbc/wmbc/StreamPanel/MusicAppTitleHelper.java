package edu.umbc.wmbc.StreamPanel;

import javafx.application.Platform;
import javafx.scene.control.Label;

import java.io.IOException;
import java.util.TimerTask;

public class MusicAppTitleHelper extends TimerTask {
    private String titleString;
    private Label detectedLabel;
    public MusicAppTitleHelper(String titleString, Label detectedLabel){
        this.titleString = titleString;
        this.detectedLabel = detectedLabel;
    }
    @Override
    public void run() {
        StatusGrabber sg = new StatusGrabber();
        try {
            try {
                titleString = sg.currentTitle();
                StreamPanel.setkMusicPlayerTitle(titleString);
            }catch(NullPointerException ignored){}
        }catch(IOException ignored){}
        Platform.runLater(
                () -> {
                    detectedLabel.setText(titleString);
                }
        );


    }
}
