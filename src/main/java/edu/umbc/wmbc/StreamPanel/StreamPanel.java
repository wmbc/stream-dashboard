package edu.umbc.wmbc.StreamPanel;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;


public class StreamPanel extends Application {
    private String kCurrentTitle = "WMBC";
    private static String kMusicPlayerTitle = "WMBC";
    private Label resultsLabel;

    public void start(Stage stage) {
        //Create the containers for our visual objects. Set padding and spacing to avoid crashing into other things.
        //Vbox lays all items inside it vertically in one column.
        //HBox lays all items inside it horizontally on one row.
        //Combining a "main" VBox with several "intermediate" HBoxes will be the primary method of adding UI elements here.
        //However, we will make use of a GridPane later, which allows us to specify arbitrary positions for individual elements.
        VBox vb = new VBox(20);
        HBox statusLeds = new HBox(100);
        statusLeds.setPadding(new Insets(5, 0, 5, 40));
        HBox statusLabels = new HBox(20);
        statusLabels.setPadding(new Insets(0, 0, 0, 15));
        vb.setPadding(new Insets(5, 5, 5, 10));

        //Start creating our interface objects.
        Label title = new Label("Status");
        title.setFont(Font.font(18));
        //First, create our status LEDs.
        Circle streamServerStatus = new Circle(20, Color.WHITE);
        //Circle autoDJStatus = new Circle(20, Color.WHITE);
        Circle clientStreamStatus = new Circle(20, Color.WHITE);
        Circle recordingStatus = new Circle(20, Color.WHITE);
        //autoDJStatus.setStroke(Color.BLACK);
        streamServerStatus.setStroke(Color.BLACK);
        clientStreamStatus.setStroke(Color.BLACK);
        recordingStatus.setStroke(Color.BLACK);
        //Add the status LEDs to the status LED pane.
        statusLeds.getChildren().addAll(streamServerStatus, clientStreamStatus, recordingStatus);
        //Add status labels to the label pane.
        Label streamLabel = new Label("Server Ping");
        streamLabel.setFont(Font.font(20));
        //Label autoDJLabel = new Label("AutoDJ");
        //autoDJLabel.setFont(Font.font(20));
        Label clientStreamStatusLabel = new Label("Server Stream");
        clientStreamStatusLabel.setFont(Font.font(20));
        Label recordingStatusLabel = new Label("Recording Status");
        recordingStatusLabel.setFont(Font.font(20));
        statusLabels.getChildren().addAll(streamLabel, clientStreamStatusLabel, recordingStatusLabel);

        //Create a separator, to make a line in the program. This separates the informational part from the
        //interactive part.
        Separator sp = new Separator();

        //Create another HBox to lay out our Current Title display.
        //We add two labels: one static and one linked to the kCurrentTitle variable. The dynamic label is presented
        //in monospace to indicate it will change.
        HBox nowPlaying = new HBox(5);
        Label titleLabel = new Label("Current Title: ");
        titleLabel.setFont(Font.font(20));
        Label currentTitle = new Label(kCurrentTitle);
        currentTitle.setFont(Font.font("Monospace", 25));
        nowPlaying.getChildren().addAll(titleLabel, currentTitle);

        //Create a grid layout for the configuration options.
        GridPane titleSourceGrid = new GridPane();
        titleSourceGrid.setHgap(10);
        titleSourceGrid.setVgap(10);

        //We will use two RadioButtons, two static Labels, and a dynamic Label in this section.
        //Create a simple label to describe the next part of the program.
        Label pullTitleFromLabel = new Label("Pull Title From:");
        pullTitleFromLabel.setFont(Font.font(20));
        //Declare radio buttons and increase their default font sizes.
        RadioButton autoDJRB = new RadioButton("AutoDJ");
        autoDJRB.setFont(Font.font(15));
        RadioButton customRB = new RadioButton("Custom");
        customRB.setFont(Font.font(15));
        //RadioButtons must be set as part of a ToggleGroup of mutually exclusive options. These two buttons are
        //mutually exclusive, so they will go into the same ToggleGroup.
        ToggleGroup titleChoiceGrp = new ToggleGroup();
        autoDJRB.setToggleGroup(titleChoiceGrp);
        customRB.setToggleGroup(titleChoiceGrp);

        //Declare our labels. Again, the dynamic label is set in monospace to show it will change.
        Label detectedLabel = new Label("Detected this title from AutoDJ: ");
        detectedLabel.setFont(Font.font(15));
        Label detectedTitleLabel = new Label(kMusicPlayerTitle);
        detectedTitleLabel.setFont(Font.font("Monospace", 20));

        //Add our objects to the grid. (0,0) is the top-leftmost position, and column comes first in the arguments.
        titleSourceGrid.add(pullTitleFromLabel, 0, 0);
        titleSourceGrid.add(autoDJRB, 0, 1);
        titleSourceGrid.add(detectedLabel, 1, 1);
        titleSourceGrid.add(detectedTitleLabel, 2, 1);
        titleSourceGrid.add(customRB, 0, 2);

        //Create a new grid for our custom setting and text entry.
        GridPane customEntryGrid = new GridPane();
        customEntryGrid.setHgap(20);
        customEntryGrid.setVgap(10);
        //We will use one static Label and a Text Entry for this section.
        Label customEntryLabel = new Label("Custom Entry:");
        customEntryLabel.setFont(Font.font(20));
        TextField customEntryField = new TextField();
        customEntryField.setPrefColumnCount(50);

        //Add items to the grid.
        customEntryGrid.add(customEntryLabel, 0, 0);
        customEntryGrid.add(new Label("Enter in 'Title - Artist' form, eg. 'Bohemian Rhapsody - Queen'."), 0, 1);
        customEntryGrid.add(customEntryField, 0, 2);

        //A hidden label with no text is placed here, to be able to indicate results of previous operation.
        resultsLabel = new Label();
        resultsLabel.setFont(Font.font(16));

        //Final item: a submit button. We'll place this in its own HBox so that we can center it.
        HBox submitBox = new HBox();
        submitBox.setAlignment(Pos.CENTER);
        Button submitBtn = new Button("Submit");
        submitBtn.setFont(Font.font(30));
        submitBtn.setAlignment(Pos.CENTER);
        submitBox.getChildren().addAll(submitBtn);



        //Define some behavior for the previous objects. If we select the "auto dj" source button, we want to
        //disable the text field so mistaken entry does not occur. I don't know how the lambda works, don't ask me.
        autoDJRB.setOnAction(event -> customEntryField.setDisable(true));
        customRB.setOnAction(event -> customEntryField.setDisable(false));
        //On startup we should default to AutoDJ.
        autoDJRB.setSelected(true);
        customEntryField.setDisable(true);
        //When we click submit, we want to update the data.
        submitBtn.setOnAction(event -> {
//            if(autoDJRB.isSelected()){
//                customEntryField.clear();
//                kCurrentTitle = kMusicPlayerTitle;
//                currentTitle.setText(kCurrentTitle);
//
//            }
            if (sanitize(customEntryField.getCharacters().toString())) {
//                if (autoDJRB.isSelected()) {
//                    kCurrentTitle = kMusicPlayerTitle;
//                } else {
                    kCurrentTitle = customEntryField.getText();
//                }
                currentTitle.setText(kCurrentTitle);
                try {
                    ProcessBuilder titlePB = new ProcessBuilder("butt","-u",kCurrentTitle);
                    titlePB.start();
                    setResults("Title set!",false);
                } catch (IOException e) {
                    setResults("Failed to set title due to system error.",true);
                }
            } else {
                setResults("Failed to set new title: Illegal characters present.\n" +
                        "The broadcasting software does not support accented characters or most special characters.",true);
            }
        });
        //Same deal with hitting "enter" on the keyboard. This should be split into its own method, but
        //updating text labels will be a royal pain to import into its scope.
        customEntryField.setOnKeyPressed(event1 -> {
            if(autoDJRB.isSelected()){
                customEntryField.clear();
            }
            if (event1.getCode() == KeyCode.ENTER) {
                if (sanitize(customEntryField.getCharacters().toString())) {
                    if (autoDJRB.isSelected()) {
                        kCurrentTitle = kMusicPlayerTitle;
                    } else {
                        kCurrentTitle = customEntryField.getText();
                    }
                    currentTitle.setText(kCurrentTitle);
                    try {
                        ProcessBuilder titlePB = new ProcessBuilder("butt","-u",kCurrentTitle);
                        titlePB.start();
                        setResults("Title set!",false);
                    } catch (IOException e) {
                        setResults("Failed to set title due to system error.",true);
                    }
                } else {
                        setResults("Failed to set new title: Illegal characters present.\n" +
                                "The broadcasting software does not support accented characters or most special characters.",true);
                }
            }
        });


        //We are adding all of our intermediate objects to the "main" pane here.
        vb.getChildren().addAll(title, statusLeds, statusLabels, sp, nowPlaying, titleSourceGrid, customEntryGrid, submitBox, resultsLabel);
        Scene scene = new Scene(vb, 900, 650);
        stage.setScene(scene);
        stage.setTitle("WMBC Stream Panel");
        stage.getIcons().add(new Image(Objects.requireNonNull(StreamPanel.class.getResourceAsStream("/WMBCLogo.png"))));
        stage.show();

        //Maintenance functions - this section runs the "Stream" status light.
        //Java's Timer function runs the check in TaskHelper.java once every ten seconds. The check spins off
        //an instance of the StreamListener
        Timer streamCheck = new Timer(true);
        StreamPingTaskHelper streamCheckHelper = new StreamPingTaskHelper("172.16.18.2", 8080, streamServerStatus);
        streamCheck.schedule(streamCheckHelper, 0, 10000);
        Timer clientCheck = new Timer(true);
        ClientStreamTaskHelper streamClientChecker = new ClientStreamTaskHelper(clientStreamStatus);
        clientCheck.schedule(streamClientChecker, 200, 5000);
        Timer recordingCheck = new Timer(true);
        RecordingTaskHelper recordingChecker = new RecordingTaskHelper(recordingStatus);
        recordingCheck.schedule(recordingChecker, 2000, 5000);
        Timer titleCheck = new Timer(true);
        MusicAppTitleHelper titleGrabber = new MusicAppTitleHelper(kMusicPlayerTitle, detectedTitleLabel);
        titleCheck.schedule(titleGrabber,500,2000);
       Timer autoSubmit = new Timer(true);
       TimerTask autoSubmitTask = new TimerTask() {
           @Override
           public void run() {
               if(autoDJRB.isSelected() && !kCurrentTitle.equals(kMusicPlayerTitle)){

                   kCurrentTitle = kMusicPlayerTitle;

                   Platform.runLater(() ->
                   {currentTitle.setText(kCurrentTitle);
                       try {
                           ProcessBuilder titlePB = new ProcessBuilder("butt","-u",kCurrentTitle);
                           titlePB.start();
                           setResults("Title updated from AutoDJ!",false);
                       } catch (IOException e) {
                           setResults("Failed to set title due to system error.",true);
                       }
                           });
                           }
           }
       };
       autoSubmit.schedule(autoSubmitTask,5000,2000);


    }

    private boolean sanitize(String message) {
        return message.matches("[\\w*\\s*\\-*0-9*.*]*");
    }
    private void setResults(String message, boolean isFailure){
        if(isFailure){
            resultsLabel.setText(message);
            resultsLabel.setTextFill(Color.RED);
            Timer clearTimer = new Timer(true);
            TimerTask clearTask = new TimerTask() {
                @Override
                public void run() {
                    Platform.runLater(() ->{
                                resultsLabel.setText("");
                            }
                    );

                }
            };
            clearTimer.schedule(clearTask,5000);
        }else{
            resultsLabel.setText(message);
            resultsLabel.setTextFill(Color.GREEN);
            Timer clearTimer = new Timer(false);
            TimerTask clearTask = new TimerTask() {
                @Override
                public void run() {
                    Platform.runLater(() ->{
                                resultsLabel.setText("");
                            }
                    );
                }
            };
            clearTimer.schedule(clearTask,5000);
        }
    }
    public static String getkMusicPlayerTitle() {
        return kMusicPlayerTitle;
    }

    public static void setkMusicPlayerTitle(String kMusicPlayerTitleInput) {
        kMusicPlayerTitle = kMusicPlayerTitleInput;
    }



}
