module edu.umbc.wmbc.StreamPanel {
    requires javafx.controls;
    requires java.net.http;

    exports edu.umbc.wmbc.StreamPanel;
}